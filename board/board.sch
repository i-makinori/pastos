EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:DB15_Female_HighDensity J?
U 1 1 5F6BFA7A
P 2150 2900
F 0 "J?" H 2150 3767 50  0000 C CNN
F 1 "DB15_Female_HighDensity" H 2150 3676 50  0000 C CNN
F 2 "Connector_Dsub:DSUB-15-HD_Female_Horizontal_P2.29x1.98mm_EdgePinOffset3.03mm_Housed_MountingHolesOffset4.94mm" H 1200 3300 50  0001 C CNN
F 3 " ~" H 1200 3300 50  0001 C CNN
	1    2150 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Mini-DIN-6 J?
U 1 1 5F6C369D
P 2050 4300
F 0 "J?" H 2050 4667 50  0000 C CNN
F 1 "Mini-DIN-6" H 2050 4576 50  0000 C CNN
F 2 "" H 2050 4300 50  0001 C CNN
F 3 "http://service.powerdynamics.com/ec/Catalog17/Section%2011.pdf" H 2050 4300 50  0001 C CNN
	1    2050 4300
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J?
U 1 1 5F6C4D20
P 2050 5350
F 0 "J?" H 2107 5817 50  0000 C CNN
F 1 "USB_B_Micro" H 2107 5726 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex_47346-0001" H 2200 5300 50  0001 C CNN
F 3 "~" H 2200 5300 50  0001 C CNN
	1    2050 5350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
