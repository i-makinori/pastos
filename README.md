# Past Os

これは、ノイマン型コンピュータによる、正しきコンピュータを探す取り組みであって、自己矛盾でもある。


# 機能

### hardware

- VGA output
- PS/2 keyboard
- mouse
- serial communication
- SD-Card
- GPIOs


### language

- lisp
- prolog
- mathematics, , ploting
- proof


### software

- REPL means Read Eval Print Loop
- Meta-macs (Emacs REPL as frame or window of this system)
- Serial Communitation Interface REPL
- Document, Program Editor
- 代数方程式解析 射影プログラム
- 幾何学証明 射影プログラム
- 物理モデル解析 射影プログラム
- 証明検証プログラム
- 不完全性定理
