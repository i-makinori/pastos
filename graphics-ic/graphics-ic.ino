// memo
//
// $ sudo chmod a+rw /dev/ttyUSB0
// $ arduino-cli compile --fqbn arduino:avr:nano:cpu=atmega328old graphics-ic
// $ arduino-cli upload -p /dev/ttyUSB0 --fqbn arduino:avr:nano:cpu=atmega328old graphics-ic
//



void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(50);
    digitalWrite(LED_BUILTIN, LOW);
    delay(50);
}
